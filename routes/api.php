<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::apiResource('/teacher', 'TeacherController');
Route::apiResource('/lesson', 'LessonController');
Route::get('/teacher_detail', 'LessonDetailController@index');
Route::get('/teacher_detail/{teacher}', 'LessonDetailController@show');
Route::apiResource('/student', 'StudentController');
Route::apiResource('/qBank', 'QBankController');
Route::apiResource('/examinee', 'ExamineeController');
Route::apiResource('/exam', 'ExamController');
Route::apiResource('/question', 'QuestionController');