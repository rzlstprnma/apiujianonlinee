<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'nisn', 'student_class_id', 'name', 'birth_place', 'birth_date', 'gender', 'religion', 'password', 'pict'
    ];

    public function studentClass()
    {
        return $this->belongsTo('App\StudentClass', 'student_class_id');
    }
}
