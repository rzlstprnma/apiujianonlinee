<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentClass extends Model
{
    protected $fillable = [
        'majors_code', 'level', 'name'
    ];

    public function exam()
    {
        return $this->belongsToMany('App\Exam', 'class_examinees', 'student_class_id');
    }

    public function students()
    {
        return $this->hasMany('App\Student', 'student_class_id', 'id');
    }
}
