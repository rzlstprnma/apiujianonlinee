<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'question_bank_id', 'question_number', 'question', 'answer',
        'optA', 'optB', 'optC', 'optD', 'optE'
    ];

    public function qBank(){
        return $this->belongsTo('App\QuestionBank', 'question_bank_id', 'id');
    }
}
