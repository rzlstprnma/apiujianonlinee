<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionBank extends Model
{
    protected $fillable = [
        'teacher_id', 'level', 'name', 'total_qustions', 'total_options', 'status'
    ];

    public function teacher()
    {
        return $this->belongsTo('App\Teacher', 'teacher_id', 'id');
    }

    public function questions()
    {
        return $this->hasMany('App\Question', 'question_bank_id', 'id');
    }
}
