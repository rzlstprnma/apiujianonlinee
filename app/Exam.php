<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable = [
        'question_bank_id', 'token_id', 'time'
    ];

    public function qBank()
    {
        return $this->belongsTo('App\QuestionBank', 'question_bank_id');
    }

    public function sClass()
    {
        return $this->belongsToMany('App\StudentClass', 'class_examinees', 'exam_id');
    }

    public function sExaminees()
    {
        return $this->belongsToMany('App\Student', 'student_examinees', 'exam_id');
    }
    
    public function token(){
        return $this->belongsTo('App\Token');
    }

    public function questions()
    {
        return $this->hasMany('App\Question', 'question_bank_id', 'question_bank_id');
    }
}
