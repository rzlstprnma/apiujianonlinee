<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LessonDetail extends Model
{
    protected $fillable = [
        'teacher_id', 'lesson_id'
    ];

    public function teacher()
    {
        return $this->belongsTo('App\Teacher', 'teacher_id');
    }

    public function lesson()
    {
        return $this->belongsTo('App\Lesson', 'lesson_id');
    }

    protected $table = 'lesson_details';
}
