<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Examinee extends Model
{
    protected $fillable = [
        'student_id', 'exam_id', 'session', 'start_at'
    ];

    protected $table = 'student_examinees';

    public function student()
    {
        return $this->belongsTo('App\Student', 'student_id');
    }
}
