<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\StudentResource;
use App\Http\Resources\StudentResourceCollection;
use App\Student;

class StudentController extends Controller
{
    public function login()
    {
        return view('student.login');
    }

    public function index()
    {
        return new StudentResourceCollection(Student::paginate());
    }

    public function show(Student $student)
    {
        StudentResource::withoutWrapping();
        return new StudentResource($student);
    }
}
