<?php

namespace App\Http\Controllers;

use App\Exam;
use App\Http\Controllers\Controller;
use App\Http\Resources\ExamResource;
use App\Http\Resources\ExamResourceCollection;
use Illuminate\Http\Request;

class ExamController extends Controller
{
    public function index(){
        return new ExamResourceCollection(Exam::all());
    }

    public function show(Exam $exam){
        ExamResource::withoutWrapping();
        return new ExamResource($exam);
    }
}
