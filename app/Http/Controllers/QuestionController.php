<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\QuestionResource;
use App\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function show(Question $question){
        QuestionResource::withoutWrapping();
        return new QuestionResource($question);
    }
}
