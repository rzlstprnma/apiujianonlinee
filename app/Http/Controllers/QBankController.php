<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\QuestionBank;
use App\Http\Resources\QBankResource;
use App\Http\Resources\QBankResourceCollection;

class QBankController extends Controller
{
    public function index()
    {
        return new QBankResourceCollection(QuestionBank::paginate());
    }

    public function show($id)
    {
        $qbank= QuestionBank::find($id)->firstOrFail();
        QBankResource::withoutWrapping();
        return new QBankResource($qbank);
    }
}
