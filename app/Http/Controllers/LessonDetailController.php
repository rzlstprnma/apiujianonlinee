<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\LessonDetail;
use App\Teacher;
use App\Http\Resources\LessonDResource;
use App\Http\Resources\LessonDResourceCollection;

class LessonDetailController extends Controller
{
    public function index()
    {
        return new LessonDResourceCollection(Teacher::paginate());
    }

    public function show(Teacher $teacher)
    {
        LessonDResource::withoutWrapping();
        return new LessonDResource($teacher);
    }
}
