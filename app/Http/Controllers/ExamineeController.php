<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\ExamineeResource;
use App\Http\Resources\ExamineeResourceCollection;
use App\Exam;

class ExamineeController extends Controller
{
    public function index()
    {
        return new ExamineeResourceCollection(Exam::paginate());
    }

    public function show(Exam $exam)
    {
        ExamineeResource::withoutWrapping();
        return new ExamineeResource($exam);
    }
}
