<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExamineeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request, $this->qBank, $this->sExaminees, $this->questions);
        // return[
        //     "exam_name" => $this->qBank->name,
        //     "examinees" => $this->sExaminee
        // ];
    }
}
