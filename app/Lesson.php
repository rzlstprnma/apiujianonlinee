<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $fillable = [
        'id', 'name'
    ];

    public function teachers()
    {
        $this->belongsToMany('App\Teacher', 'lesson_details', 'lesson_id');
    }

    public function lessonDetail()
    {
        $this->hasMany('App\LessonDetail');
    }
}
