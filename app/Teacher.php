<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = [
        'nip', 'user_id', 'name', 'gender', 'religion', 'address',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function lessons()
    {
        return $this->belongsToMany('App\Lesson', 'lesson_details', 'teacher_id');
    }

    public function lessonDetail()
    {
        return $this->hasMany('App\LessonDetail');
    }
}
